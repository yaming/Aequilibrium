public class Transformers {
	private String name;
	private String allegiance;
	private int strength;
	private int intelligence;
	private int speed;
	private int endurance;
	private int rank;
	private int courage;
	private int firepower;
	private int skill;
	private int overallRating;
	private boolean eliminated;
	
	public Transformers(String[] str){
		Transformers(str[0].trim(),str[1].trim(),Integer.parseInt(str[2].trim()),Integer.parseInt(str[3].trim()),Integer.parseInt(str[4].trim()),Integer.parseInt(str[5].trim()),Integer.parseInt(str[6].trim()),Integer.parseInt(str[7].trim()),Integer.parseInt(str[8].trim()),Integer.parseInt(str[9].trim()));
	}
	
	public void Transformers(String name,String allegiance,int strength,int intelligence,int speed,int endurance,int rank,int courage,int firepower,int skill){
		setName(name);
		setAllegiance(allegiance);
		setStrength(strength);
		setIntelligence(intelligence);
		setSpeed(speed);
		setEndurance(endurance);
		setRank(rank);
		setCourage(courage);
		setFirepower(firepower);
		setSkill(skill);
		setOverallRating(strength + intelligence + speed + endurance + firepower);
		setEliminated(false);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllegiance() {
		return allegiance;
	}

	public void setAllegiance(String allegiance) {
		this.allegiance = allegiance;
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public int getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(int intelligence) {
		this.intelligence = intelligence;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getEndurance() {
		return endurance;
	}

	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getCourage() {
		return courage;
	}

	public void setCourage(int courage) {
		this.courage = courage;
	}

	public int getFirepower() {
		return firepower;
	}

	public void setFirepower(int firepower) {
		this.firepower = firepower;
	}

	public int getSkill() {
		return skill;
	}

	public void setSkill(int skill) {
		this.skill = skill;
	}

	public int getOverallRating() {
		return overallRating;
	}

	public void setOverallRating(int overallRating) {
		this.overallRating = overallRating;
	}

	public boolean isEliminated() {
		return eliminated;
	}

	public void setEliminated(boolean eliminated) {
		this.eliminated = eliminated;
	}
	
}
