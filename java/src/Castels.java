import java.util.Scanner;

public class Castels {
	private static int flag=0;
	private static int lastFlag=0;
	private static int changeFlag=0;
	private static boolean isPeakOrValley=true;
	
	private static int builtCastels(int[] array){
		int castelsTotal=0;
		if(array.length==0)
			return castelsTotal;
		flag=0;
		lastFlag=0;
		changeFlag=0;
		isPeakOrValley=true;
		int currentLand=array[0];
		int lastLand=array[0];
		for(int i=0;i<array.length;i++){
			currentLand=array[i];
			if(currentLand-lastLand ==0)
				flag=0;
			else if(currentLand-lastLand>0)
				flag=1;
			else if(currentLand-lastLand<0)
				flag=-1;
			
			if (i == array.length - 1) { 
				if (flag != 0 || changeFlag != 0 ) {					
					castelsTotal++;
					isPeakOrValley=false;
				};				
			}
			
			if(checkNext()) {
				lastFlag = flag;	
				lastLand = currentLand;
				
				castelsTotal++;
				isPeakOrValley=false;
				continue;
			}
			if(flag>0){
				changeFlag = 1;
				if (lastFlag == -1) {
					isPeakOrValley = true;
				}
			}else if(flag<0){
				changeFlag = -1;
				if (lastFlag == 1) {
					isPeakOrValley = true;
				};
			}
			if(isPeakOrValley){
				lastFlag = flag;	
				lastLand = currentLand;
				castelsTotal++;
				isPeakOrValley=false;
				continue;
			}

			lastFlag = flag;	
			lastLand = currentLand;
			
		}
		return castelsTotal;
	}
	
	private static boolean checkNext(){
		boolean re=false;
		if (changeFlag == -1 && flag == 1) {
			isPeakOrValley = true;
			changeFlag = 1;
			re = true;
		} else if(changeFlag == 1 && flag == -1){
			isPeakOrValley = true;
			changeFlag = -1;
			re = true;
		}
		return re;
	}
	
	
	
	public static void main(String[] arg){
		int[] test={1,2,3,4,4,5};
		System.out.println("Please type array of integers.");
		Scanner sc = new Scanner(System.in); 
		while(true){
			try{
				String input = sc.nextLine(); 
				if(input.equalsIgnoreCase("exit")){
					System.out.println("\nBye!");
					System.exit(0);
				}
		        String [] strArray= input.split(",");  
		        int[] intArray = new int[strArray.length];  
		        for(int i=0; i<strArray.length;i++){  
		        	intArray[i] =Integer.parseInt(strArray[i]);
		        }	
		        System.out.println("["+input+"] You can build "+builtCastels(intArray)+" castels!\n");
			}catch(Exception e){
				System.out.println("\nError:You can only type array of integers.");
			}
		}
		
        
		
		
	 
	}
 
 
}
