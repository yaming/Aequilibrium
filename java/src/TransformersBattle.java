import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TransformersBattle {
	private ArrayList<Transformers> teamA;
	private ArrayList<Transformers> teamD;
	private int battleCount;
	private int AWin;
	private int DWin;
	private StringBuilder AwinNames;
	private StringBuilder DwinNames;
	private StringBuilder survivorNames;
	
	public void recordWin(Transformers win){
		if(win.getAllegiance().equalsIgnoreCase("A")){
			AwinNames.append(win.getName()).append(",");
			AWin++;
		}else if(win.getAllegiance().equalsIgnoreCase("D")){
			DwinNames.append(win.getName()).append(",");
			DWin++;
		}
	}
	public  void courageAndStrengthRule(Transformers first, Transformers second){
		if(first.isEliminated() || second.isEliminated())
			return;
		if(first.getCourage()-second.getCourage()>=4 &&
				first.getStrength()-second.getStrength()>=3){
			second.setEliminated(true);
			recordWin(first);
		}else if(second.getCourage()-first.getCourage()>=4 &&
					second.getStrength()-first.getStrength()>=3){
			first.setEliminated(true);
			recordWin(second);
		}
		
	}
	public void skillRule(Transformers first, Transformers second){
		if(first.isEliminated() || second.isEliminated())
			return;
		if (first.getSkill()-second.getSkill() >= 3) {
			second.setEliminated(true);
			recordWin(first);
		}else if (second.getSkill()-first.getSkill()>=3){
			first.setEliminated(true);
			recordWin(second);
		}
		
	}
	
	public void leaderRule(Transformers first,Transformers second){
		if(first.isEliminated() || second.isEliminated())
			return;
		if(first.getName().equalsIgnoreCase("Optimus Prime") && !second.getName().equalsIgnoreCase("Predaking")){
			second.setEliminated(true);
			recordWin(first);
		}if(second.getName().equalsIgnoreCase("Predaking") && !first.getName().equalsIgnoreCase("Optimus Prime")){
			first.setEliminated(true);
			recordWin(second);
		}if(second.getName().equalsIgnoreCase("Predaking") && first.getName().equalsIgnoreCase("Optimus Prime")){
			first.setEliminated(true);
			second.setEliminated(true);
		}
	}
	
	public void overallRule(Transformers first,Transformers second){
		if(first.isEliminated() || second.isEliminated())
			return;
		if(first.getOverallRating()-second.getOverallRating()>0){
			second.setEliminated(true);
			recordWin(first);
		}else if(second.getOverallRating()-first.getOverallRating()>0){
			first.setEliminated(true);
			recordWin(second);
		}else if(first.getOverallRating()-second.getOverallRating()==0){
			first.setEliminated(true);
			second.setEliminated(true);
		}
			
	}	
	
	public void battle(Transformers t1,Transformers t2){
		
		// is Optimus Prime or Predaking
		leaderRule(t1,t2);
		//courage and Strength rule
		courageAndStrengthRule(t1,t2);
		//skill rule
		skillRule(t1,t2);
		//overall rule
		overallRule(t1,t2);
		this.battleCount ++;
		if(t1.isEliminated() && t2.isEliminated())
			System.out.println(t1.getName()+" VS "+t2.getName()+"    all defeat");
		else
			System.out.println(t1.getName()+" VS "+t2.getName()+"    "+(t1.isEliminated()?(t2.getName()+" win"):(t1.getName()+" win")));
	}
	
	public void startBattle(){
		battleCount=0;
		AWin=0;
		DWin=0;
		AwinNames=new StringBuilder();
		DwinNames=new StringBuilder();
		survivorNames = new StringBuilder();
		System.out.println("=============================================");
		System.out.println("=============================================");
		for(int i=0;i<teamA.size();i++){
			if(i>=teamD.size())
				break;
			battle(teamA.get(i),teamD.get(i));
			
		}
		System.out.println("=============================================");
		System.out.println("=============================================");
		System.out.println(battleCount+" battles");
		if(DWin>AWin){
			for(Transformers t:teamA){
				if(!t.isEliminated())
					survivorNames.append(t.getName()).append(",");
					
			}
			System.out.println("Winning team (Decepticons):"+ DwinNames.deleteCharAt(DwinNames.length()-1).toString());
			if(survivorNames!=null &&survivorNames.length()>0)
				System.out.println("Survivors from the losing team (Autobots):"+ survivorNames.deleteCharAt(survivorNames.length()-1).toString());
		}else if(AWin>DWin){
			for(Transformers t:teamD){
				if(!t.isEliminated())
					survivorNames.append(t.getName()).append(",");
					
			}
			System.out.println("Winning team (Autobot ):"+ AwinNames.deleteCharAt(AwinNames.length()-1).toString());
			if(survivorNames!=null &&survivorNames.length()>0)
				System.out.println("Survivors from the losing team (Decepticons):"+ survivorNames.deleteCharAt(survivorNames.length()-1).toString());
		}else if(AWin==DWin){
			System.out.println("All the transformers have been destroyed.");
		}
		System.out.println("=============================================");
		System.out.println("=============================================");
	}
	
	@SuppressWarnings("unchecked")
	public void initTeam(File file){
        try{
        	teamA = new ArrayList<Transformers>();
        	teamD = new ArrayList<Transformers>();
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s = null;
            while((s = br.readLine())!=null){
            	String[] line=s.split(",");
            	Transformers temp=new Transformers(line);
            	if(temp.getAllegiance().equalsIgnoreCase("A")){
            		teamA.add(temp);
            	}else if(temp.getAllegiance().equalsIgnoreCase("D")){
            		teamD.add(temp);
            	}            		
            }
            Collections.sort(teamA, new SortByRank()); //sort by rank
            Collections.sort(teamD, new SortByRank()); //sort by rank
            br.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
	
	public static void main(String[] arg){
		
		File team1File = new File("Transformers.txt");
		TransformersBattle battle=new TransformersBattle();
		//init team data
		battle.initTeam(team1File);
		battle.startBattle();
		//start battle
	}
	
	class SortByRank implements Comparator {
        public int compare(Object o1, Object o2) {
        	Transformers t1 = (Transformers) o1;
        	Transformers t2 = (Transformers) o2;
        	if (t1.getRank() < t2.getRank())
        		return 1;
        	return -1;
        }
       }
}

