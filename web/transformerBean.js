﻿function TransformerBean(str){ 
	var array= str.split(",");
	
	this.name=array[0].trim();
	this.allegiance=array[1].trim();
	this.strength=parseInt(array[2].trim());
	this.intelligence=parseInt(array[3].trim());
	this.speed=parseInt(array[4].trim());
	this.endurance=parseInt(array[5].trim());
	this.rank=parseInt(array[6].trim());
	this.courage=parseInt(array[7].trim());
	this.firepower=parseInt(array[8].trim());
	this.skill=parseInt(array[9].trim());
	this.overallRating=this.strength + this.intelligence + this.speed + this.endurance + this.firepower;
	this.eliminated=false;

	}
