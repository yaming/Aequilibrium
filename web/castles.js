﻿var app = angular.module('myApp', []);
app.controller('castles', function($scope) {
    $scope.getInput = function(){
    	
    	$scope.output=chkInput($scope.inputArray)?$scope.inputArray+" You can build "+getCastles($scope.inputArray)+" castles!":"Error:You can only type array of integers.";
    }
});

function chkInput(str){
 var reNum=/^[0-9,]+$/;
 return(reNum.test(str));
}
 
function getCastles(str){	
		var arrayStr= new Array(); 
		arrayStr=str.split(',');
		var array=[];
		for (i=0;i<arrayStr.length;i++){
    	array[i] = parseInt(arrayStr[i]);
		} 
		this.castelsTotal=0;
		if(array.length==0)
			return this.castelsTotal;
		this.flag=0;
		this.lastFlag=0;
		this.changeFlag=0;
		this.isPeakOrValley=true;
		this.currentLand=array[0];
		this.lastLand=array[0];
		for(var i=0;i<array.length;i++){
			this.currentLand=array[i];
			if(this.currentLand-this.lastLand ==0)
				this.flag=0;
			else if(this.currentLand-this.lastLand>0)
				this.flag=1;
			else if(this.currentLand-this.lastLand<0)
				this.flag=-1;
			
			if (i == array.length - 1) { 
				if (this.flag != 0 || this.changeFlag != 0 ) {					
					this.castelsTotal++;
					this.isPeakOrValley=false;
				};				
			}
			
			if(checkNext()) {
				this.lastFlag = this.flag;	
				this.lastLand = this.currentLand;
				
				this.castelsTotal++;
				this.isPeakOrValley=false;
				continue;
			}
			if(this.flag>0){
				this.changeFlag = 1;
				if (this.lastFlag == -1) {
					this.isPeakOrValley = true;
				}
			}else if(this.flag<0){
				this.changeFlag = -1;
				if (this.lastFlag == 1) {
					this.isPeakOrValley = true;
				};
			}
			if(this.isPeakOrValley){
				this.lastFlag = this.flag;	
				this.lastLand = this.currentLand;
				this.castelsTotal++;
				this.isPeakOrValley=false;
				continue;
			}

			this.lastFlag = this.flag;	
			this.lastLand = this.currentLand;
			
		}		
		return this.castelsTotal;
}

function checkNext(){
		var re=false;
		if (this.changeFlag == -1 && this.flag == 1) {
			this.isPeakOrValley = true;
			this.changeFlag = 1;
			re = true;
		} else if(this.changeFlag == 1 && this.flag == -1){
			this.isPeakOrValley = true;
			this.changeFlag = -1;
			re = true;
		}
		return re;
	}