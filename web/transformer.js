﻿var app2 = angular.module('myApp2', []);
var teamA;
var teamD;
var battleCount;
var AwinNames;
var DwinNames;
var survivorNames;
var AWin;
var DWin;
var outputStr;

app2.controller('transformer', function($scope,$sce) {
    $scope.getTransInput = function(){
    	teamA = new Array();
    	teamD = new Array();
    	
    	try{
    		initData($scope.inputTrans);
    		startBattle();
    		$scope.html = outputStr;
				$scope.transOutput = $sce.trustAsHtml($scope.html);    		
    	}catch(error){
    		alert(error);
				
    	}
    	
    }
});

function initData(input){
	try{
		var temp=input.split("\n");
		for(var i=0;i<temp.length;i++){
		var obj=new TransformerBean(temp[i]);
		if(obj.allegiance=="A")
			teamA.push(obj);
		else if(obj.allegiance=="D")
			teamD.push(obj);
	}
	teamA.sort(function(a, b){
    return b.rank > a.rank;	
	});
  teamD.sort(function(a, b){
    return b.rank > a.rank;	
	});
	}catch(error){
		throw new Error("Team Data Error!")
	}
	
}

function startBattle(){
		battleCount=0;
		AWin=0;
		DWin=0;
		AwinNames="";
		DwinNames="";
		survivorNames="";
		outputStr="";
		for(var i=0;i<teamA.length;i++){
			if(i>=teamD.length)
				break;
			battle(teamA[i],teamD[i]);
		}
		
		
		//outputStr+=("AWin "+AWin +"<br>");
		//outputStr+=("DWin"+ DWin +"<br>");
		outputStr+="========================================<br>";
		outputStr+=("<font color='red'>"+battleCount+"</font> battles<br>");
		
		if(DWin>AWin){
			for(var i=0;i<teamA.length;i++){
				if(!teamA[i].eliminated)
					survivorNames+=teamA[i].name+",";
			}
			outputStr+=("Winning team (Decepticons):<font color='red'>"+ DwinNames.substr(0,DwinNames.length-1)+"</font><br>");			
			if(survivorNames!=null &&survivorNames.length>0)
				outputStr+="Survivors from the losing team (Autobots):<font color='red'>"+survivorNames.substr(0,survivorNames.length-1)+"</font><br>";
		}else if(AWin>DWin){
			for(var i=0;i<teamD.length;i++){
				if(!teamD[i].eliminated)
					survivorNames+=teamD[i].name+",";
			}
			outputStr+=("Winning team (Autobot):<font color='red'>"+ AwinNames.substr(0,AwinNames.length-1)+"</font><br>");			
			if(survivorNames!=null &&survivorNames!="")
				outputStr+="Survivors from the losing team (Decepticons):<font color='red'>"+survivorNames.substr(0,survivorNames.length-1)+"</font><br>";
		}else if(AWin==DWin){
			outputStr+="<font color='red'>All the transformers have been destroyed.</font><br>";
		}
}

function battle(t1,t2){
		//alert(t1.name+" VS "+t2.name);
		// is Optimus Prime or Predaking
		leaderRule(t1,t2);
		//courage and Strength rule
		courageAndStrengthRule(t1,t2);
		//skill rule
		skillRule(t1,t2);
		//overall rule
		overallRule(t1,t2);
		battleCount ++;
		if(t1.eliminated==true && t2.eliminated==true)
			outputStr+="<font color='red'>"+t1.name+"</font> VS <font color='red'>"+t2.name+"</font>:    all defeat<br>";
		else
			outputStr+="<font color='red'>"+t1.name+"</font> VS <font color='red'>"+t2.name+"</font>:    "+(t1.eliminated==true?(t2.name+" win<br>"):(t1.name+" win<br>"));
}

function recordWin(win){
		if(win.allegiance=="A"){
			AwinNames+=win.name+",";
			AWin++;
		}else if(win.allegiance=="D"){
			DwinNames+=win.name+",";
			DWin++;
		}
}
	
function courageAndStrengthRule(first, second){
		if(first.eliminated || second.eliminated)
			return;
		if(first.courage-second.courage>=4 && first.strength-second.strength>=3){
			second.eliminated=true;
			recordWin(first);
		}else if(second.courage-first.courage>=4 &&	second.strength-first.strength>=3){
			first.eliminated=true;
			recordWin(second);
		}
		
}

function skillRule(first, second){
		if(first.eliminated || second.eliminated)
			return;
		if (first.skill-second.skill >= 3) {
			second.eliminated=true;
			recordWin(first);
		}else if (second.skill-first.skill>=3){
			first.eliminated=true;
			recordWin(second);
		}
		
}
	
function leaderRule(first,second){
		if(first.eliminated || second.eliminated)
			return;
		if(first.name=="Optimus Prime" && second.name!="Predaking"){
			second.eliminated=true;
			recordWin(first);
		}if(second.name=="Predaking" && first.name!="Optimus Prime"){
			first.eliminated=true;
			recordWin(second);
		}if(second.name=="Predaking" && first.name=="Optimus Prime"){
			first.eliminated=true;
			second.eliminated=true;
		}
}
	
function overallRule(first,second){
		if(first.eliminated || second.eliminated)
			return;
		if(first.overallRating-second.overallRating>0){
			second.eliminated=true;
			recordWin(first);
		}else if(second.overallRating-first.overallRating>0){
			first.eliminated=true;
			recordWin(second);
		}else if(first.overallRating-second.overallRating==0){
			first.eliminated=true;
			second.eliminated=true;
		}
			
}	
angular.bootstrap(document.getElementById("app2div"), ['myApp2']);
