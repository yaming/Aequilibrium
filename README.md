I have implemented technical tasks you assigned me in 2 ways. Java and web application(JS+AngularJS)

Java:
(1) Castle
   You could run "Castels.class" from Command Prompt then type testing data.
   for example 1,2,3,4,5 
   If you type "exit", program will exit. Please refer to the screenshot(ScreenShot_Java_Castels.jpg).

(2) Transformers
   You could run "TransformersBattle.class" from Command Prompt.This class will read testing data from Transformers.txt(bin/Transformers.txt). You also could change testing data to test other battles if necessary.
   Please refer to the screenshot(ScreenShot_Java_Transformers.jpg).

Web:
  Open "web/test.html" with Chrome. You could type Testing data and get results.
  Please refer to the screenshot(ScreenShot_web.jpg).